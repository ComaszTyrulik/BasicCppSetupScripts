#!/bin/bash
CWD=$(dirname "$(realpath $BASH_SOURCE)")
ENV_DIR=$CWD/.env/bin/activate

python3 -m venv $CWD/.env
source $ENV_DIR && python3 -m pip install -r $CWD/requirements.txt && python3 -m pip install --upgrade pip && python3 $CWD/init_project.py

# Thanks to https://stackoverflow.com/a/61020205/7301294
# This solves a problem of unresolved external symbols when linking with spdlog
source $ENV_DIR && conan profile new default --detect  # Generates default profile detecting GCC and sets old ABI
source $ENV_DIR && conan profile update settings.compiler.libcxx=libstdc++11 default  # Sets libcxx to C++11 ABI