#!/bin/bash
CWD=$(dirname "$(realpath $BASH_SOURCE)")

source $CWD/.env/bin/activate && python3 "$@"
